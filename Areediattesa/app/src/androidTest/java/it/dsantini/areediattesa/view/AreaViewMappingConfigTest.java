package it.dsantini.areediattesa.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;
import android.util.Log;

import junit.framework.Assert;

import org.junit.Test;

import it.dsantini.areediattesa.R;
import it.dsantini.areediattesa.model.impl.AreaAttesa;
import it.dsantini.areediattesa.viewModel.helper.AreaViewMapping;
import it.dsantini.areediattesa.viewModel.helper.impl.AreaViewMappingConfig;

public class AreaViewMappingConfigTest {

    @NonNull
    private final Context context;
    @NonNull
    private final AreaViewMapping m;

    public AreaViewMappingConfigTest() {
        context = InstrumentationRegistry.getTargetContext();
        m = new AreaViewMappingConfig(context);
    }

    @Test
    public void conteggio() {
        final String TAG = "AreaViewMappingConfigTest::conteggio";
        String[] array = context.getResources().getStringArray(R.array.areaViewMapping);
        int mappa = m.getMap().size(), config = array.length;
        Log.i(TAG, "Elementi nella configurazione: " + config);
        Log.i(TAG, "Elementi nella mappa: " + mappa);
        Assert.assertEquals(config, mappa);
    }

    @Test
    public void attesa() {
        Assert.assertTrue(m.getMap().containsKey(AreaAttesa.class));
    }
}
