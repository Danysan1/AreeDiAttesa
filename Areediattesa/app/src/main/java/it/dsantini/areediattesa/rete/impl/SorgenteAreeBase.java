package it.dsantini.areediattesa.rete.impl;

import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.Confine;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.rete.DownloadException;
import it.dsantini.areediattesa.rete.SorgenteAree;

public abstract class SorgenteAreeBase implements SorgenteAree {
    @Override
    @NonNull
    public GruppoAree download(@NonNull Confine c) throws DownloadException {
        if (c == null)
            throw new IllegalArgumentException("c == null");

        return c.accept(this);
    }
}
