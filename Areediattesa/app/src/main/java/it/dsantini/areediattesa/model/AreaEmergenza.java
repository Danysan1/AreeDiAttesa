package it.dsantini.areediattesa.model;

import android.support.annotation.NonNull;

import java.util.Optional;

public interface AreaEmergenza {
    /**
     * @return Descrizione dell'area
     */
    @NonNull
    Optional<String> getDescrizione();

    /**
     * @return Tipi di emergenza per cui è pensata l'area
     */
    @NonNull
    TipoEmergenza[] getEmergenze();

    /**
     * @return Foto dell'area
     */
    @NonNull
    Fotografia[] getFotografie();

    /**
     * @return Geometria geografica dell'area
     */
    @NonNull
    Geometria getGeometria();

    /**
     * @return Ente gestore dell'area
     */
    @NonNull
    Optional<String> getGestore();

    /**
     * @return Nome dell'area
     */
    @NonNull
    String getNome();
}
