package it.dsantini.areediattesa.persistenza.impl;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import it.dsantini.areediattesa.model.impl.AreaAttesa;
import it.dsantini.areediattesa.model.impl.BoundingBoxBase;
import it.dsantini.areediattesa.model.impl.FotografiaBase;
import it.dsantini.areediattesa.model.impl.GruppoAreeBase;
import it.dsantini.areediattesa.model.impl.NodoBase;
import it.dsantini.areediattesa.model.impl.Poligono;
import it.dsantini.areediattesa.model.impl.TipoEmergenzaBase;

@Database(version = 1, entities = {
        GruppoAreeBase.class,
        AreaAttesa.class,
        Poligono.class,
        NodoBase.class,
        TipoEmergenzaBase.class,
        FotografiaBase.class,
        BoundingBoxBase.class})
public abstract class MappaDatabase extends RoomDatabase {
    public abstract GruppoAreeDao gruppoAreeDao();
}
