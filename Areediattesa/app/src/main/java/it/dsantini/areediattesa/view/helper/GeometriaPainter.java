package it.dsantini.areediattesa.view.helper;

import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.Nodo;
import it.dsantini.areediattesa.model.impl.Poligono;

public interface GeometriaPainter {
    void stampa(@NonNull Nodo n);

    void stampa(@NonNull Poligono p);
}
