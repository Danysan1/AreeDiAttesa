package it.dsantini.areediattesa.model.impl;

import android.arch.persistence.room.Entity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import it.dsantini.areediattesa.model.BoundingBox;
import it.dsantini.areediattesa.model.Confine;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.rete.DownloadException;
import it.dsantini.areediattesa.rete.SorgenteAree;
import it.dsantini.areediattesa.view.helper.ConfinePainter;

@Entity(primaryKeys = {"latMin", "latMax", "lonMin", "lonMax"})
public class BoundingBoxBase extends ConfineBase implements BoundingBox {
    private final double latMin, latMax, lonMin, lonMax;

    /***
     * @param latMin Limite sud
     * @param latMax Limite nord
     * @param lonMin Limite est
     * @param lonMax Limite ovest
     * @param nome Nome del confine
     */
    public BoundingBoxBase(double latMin, double latMax, double lonMin, double lonMax, @Nullable String nome) {
        super(nome);

        //Log.v(TAG, latMin + "/" + latMin + "/" + latMax + "/" + lonMin + "/" + lonMax);

        if (latMin >= latMax)
            throw new IllegalArgumentException("latMin = " + latMin + " >= latMax =" + latMax);
        if (lonMin == lonMax)
            throw new IllegalArgumentException("lonMin == lonMax = " + lonMin);
        if (latMin < -90)
            throw new IllegalArgumentException("latMin = " + latMin + " < -90");
        if (latMax > 90)
            throw new IllegalArgumentException("latMax = " + latMax + " > +90");
        if (lonMin < -180)
            throw new IllegalArgumentException("lonMin = " + lonMin + " < -180");
        if (lonMax > 180)
            throw new IllegalArgumentException("lonMax = " + lonMax + " > +180");

        Double LatMin = latMin, LatMax = latMax, LonMin = lonMin, LonMax = lonMax;

        if (LatMin.isNaN())
            throw new IllegalArgumentException("latMin == NaN");
        if (LatMin.isInfinite())
            throw new IllegalArgumentException("latMin == infinito");
        if (LatMax.isNaN())
            throw new IllegalArgumentException("latMax == NaN");
        if (LatMax.isInfinite())
            throw new IllegalArgumentException("latMax == infinito");
        if (LonMin.isNaN())
            throw new IllegalArgumentException("lonMin == NaN");
        if (LonMin.isInfinite())
            throw new IllegalArgumentException("lonMin == infinito");
        if (LonMax.isNaN())
            throw new IllegalArgumentException("lonMax == NaN");
        if (LonMax.isInfinite())
            throw new IllegalArgumentException("lonMax == infinito");


        this.latMin = latMin;
        this.latMax = latMax;
        this.lonMin = lonMin;
        this.lonMax = lonMax;
    }

    @Override
    public double getLatMin() {
        return latMin;
    }

    @Override
    public double getLatMax() {
        return latMax;
    }

    @Override
    public double getLonMin() {
        return lonMin;
    }

    @Override
    public double getLonMax() {
        return lonMax;
    }

    @Override
    @NonNull
    public GruppoAree accept(@NonNull SorgenteAree s) throws DownloadException {
        if (s == null)
            throw new IllegalArgumentException("SorgenteAree s == null");

        return s.visit(this);
    }

    @Override
    public void accept(@NonNull ConfinePainter cp) {
        cp.stampa(this);
    }

    @Override
    public boolean contiene(@NonNull Confine contenuto) {
        if (contenuto == null)
            throw new IllegalArgumentException("contenuto == null");
        if (contenuto == this)
            return true;

        return contenuto.isContenuto(this);
    }

    @Override
    public boolean isContenuto(@NonNull BoundingBox contenente) {
        if (contenente == this)
            return true;

        if (contenente == null)
            throw new IllegalArgumentException("contenente == null");

        boolean contenutoSemplice =
                this.getLatMin() >= contenente.getLatMin() &&
                        this.getLatMax() <= contenente.getLatMax() &&
                        this.getLonMin() >= contenente.getLonMin() &&
                        this.getLonMax() <= contenente.getLonMax(),
                contenutoACavallo = false; // TODO se i bounding box sono a cavallo del meridiano opposto a Greenwich

        return contenutoSemplice || contenutoACavallo;
    }

    @Override
    public String toString() {
        return "BoundingBoxBase, latMin=" + getLatMin() + ", latMax=" + getLatMax() + ", lonMin=" + getLonMin() + ", lonMax=" + getLonMax();
    }
}
