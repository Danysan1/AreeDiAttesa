package eu.danielesantini.areediattesa.offline;

import android.util.Log;

import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.offline.OfflineRegionError;
import com.mapbox.mapboxsdk.offline.OfflineRegionStatus;

public class OfflineObserver implements OfflineRegion.OfflineRegionObserver {
    private static final String TAG = "OfflineObserver";

    @Override
    public void onStatusChanged(OfflineRegionStatus status) {

        // Calculate the download percentage
        Double percentage = status.getRequiredResourceCount() >= 0
                ? (100.0 * status.getCompletedResourceCount() / status.getRequiredResourceCount()) :
                0.0;

        if (status.isComplete()) {
            // Download complete
            Log.d(TAG, "Region downloaded successfully.");
        } else if (status.isRequiredResourceCountPrecise()) {
            Log.d(TAG, percentage.toString());
        }
    }

    @Override
    public void onError(OfflineRegionError error) {
        // If an error occurs, print to logcat
        Log.e(TAG, "onError reason: " + error.getReason());
        Log.e(TAG, "onError message: " + error.getMessage());
    }

    @Override
    public void mapboxTileCountLimitExceeded(long limit) {
        // Notify if offline region exceeds maximum tile count
        Log.e(TAG, "Mapbox tile count limit exceeded: " + limit);
    }
}
