package it.dsantini.areediattesa.viewModel.impl;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MediatorLiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Map;

import it.dsantini.areediattesa.R;
import it.dsantini.areediattesa.data.DettagliRepository;
import it.dsantini.areediattesa.data.MappaRepository;
import it.dsantini.areediattesa.data.Stato;
import it.dsantini.areediattesa.data.impl.DettagliRepositoryBase;
import it.dsantini.areediattesa.data.impl.MappaRepositoryBase;
import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.Confine;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.Mappa;
import it.dsantini.areediattesa.persistenza.Persistenza;
import it.dsantini.areediattesa.persistenza.mock.PersistenzaFallback;
import it.dsantini.areediattesa.rete.SorgenteAree;
import it.dsantini.areediattesa.rete.mock.SorgenteAreeFallback;
import it.dsantini.areediattesa.view.DettagliView;
import it.dsantini.areediattesa.viewModel.MappaViewModel;
import it.dsantini.areediattesa.viewModel.StatoDownload;
import it.dsantini.areediattesa.viewModel.helper.AreaViewMapping;
import it.dsantini.areediattesa.viewModel.helper.impl.AreaViewMappingConfig;

public class MappaViewModelBase extends AndroidViewModel implements MappaViewModel, Observer<Mappa> {
    private static final String CLASS = "MappaViewModelBase";

    @NonNull
    private final MappaRepository m;
    @NonNull
    private final DettagliRepository d;
    @NonNull
    private final MutableLiveData<AreaEmergenza[]> aree;
    @NonNull
    private final LiveData<Stato> stato;
    @NonNull
    private final MutableLiveData<StatoDownload> statoDownload;
    @NonNull
    private final Map<Class<? extends AreaEmergenza>, Class<? extends DettagliView>> mapping;
    @NonNull
    private final SorgenteAree sorgente;

    private Confine porzione;

    public MappaViewModelBase(@NonNull Application application) {
        super(application);

        this.d = DettagliRepositoryBase.getInstance();

        MappaRepositoryBase m = MappaRepositoryBase.getInstance();
        this.m = m;

        MediatorLiveData<AreaEmergenza[]> aree = new MediatorLiveData<>();
        aree.addSource(m.getMappa(), this);
        this.aree = aree;

        LiveData<Stato> stato = m.getStato();
        String nomePersistenza = getApplication().getString(R.string.persistenza);
        Log.i(CLASS, "Istanzazione di " + nomePersistenza);
        try {
            Persistenza p = (Persistenza) Class.forName(nomePersistenza).getConstructor().newInstance();
            m.setPersistenza(p);
        } catch (Exception e) {
            Log.e(CLASS, "Errore nell'istanziazione della persistenza", e);
            m.setPersistenza(new PersistenzaFallback());
            stato = new MutableLiveData<>();
            ((MutableLiveData<Stato>) stato).setValue(Stato.errore_caricamento);
        }
        this.stato = stato;

        SorgenteAree sorgente;
        String nomeSorgente = getApplication().getString(R.string.sorgenteAree);
        Log.i(CLASS, "Istanzazione di " + nomeSorgente);
        try {
            sorgente = (SorgenteAree) Class.forName(nomeSorgente).getConstructor().newInstance();
        } catch (Exception e) {
            Log.e(CLASS, "Errore nell'istanziazione della sorgente", e);
            sorgente = new SorgenteAreeFallback();
        }
        this.sorgente = sorgente;

        this.statoDownload = new MutableLiveData<>();

        AreaViewMapping avm = new AreaViewMappingConfig(getApplication());
        this.mapping = avm.getMap();
    }

    @NonNull
    @Override
    public LiveData<AreaEmergenza[]> getAree() {
        return aree;
    }

    @NonNull
    @Override
    public LiveData<Stato> getStato() {
        return stato;
    }

    @NonNull
    @Override
    public LiveData<StatoDownload> getStatoDownload() {
        return statoDownload;
    }

    private void aggiornaAree() {
        final String TAG = CLASS + "::aggiornaAree";

        Mappa map = m.getMappa().getValue();
        if (map == null)
            Log.e(TAG, "Mappa nulla");
        else if (porzione == null)
            Log.e(TAG, "Porzione nulla");
        else { // Mappa non nulla
            GruppoAree gruppo = null;

            for (GruppoAree g : map.getGruppi()) {
                if (g.getConfine().contiene(porzione)) { // Porzione di mappa contenuta nel confine del gruppo
                    gruppo = g;
                    break;
                }
            }

            if (gruppo != null) { // Gruppo trovato
                Log.v(TAG, "Trovato il gruppo con confine " + gruppo.getConfine());
                aree.setValue(gruppo.getAree());

                long eta = gruppo.getTimestamp().until(LocalDateTime.now(), ChronoUnit.DAYS),
                        etaMassima = getApplication().getResources().getInteger(R.integer.maxDays);
                if (eta >= etaMassima) { // Gruppo vecchio
                    new DownloadAsync(statoDownload, sorgente, gruppo.getConfine(), m).start();
                }
            } else { // Gruppo non trovato
                Log.v(TAG, "Gruppo non trovato, download nuovo");

                new DownloadAsync(statoDownload, sorgente, porzione, m).start();
            }
        }
    }

    @Override
    public void onSposta(@NonNull Confine porzioneDiMappa) {
        final String TAG = CLASS + "::onSposta";

        this.porzione = porzioneDiMappa;
        Log.v(TAG, "Spostamento in " + porzioneDiMappa.toString());
        aggiornaAree();
    }

    @Override
    public void onScegliArea(@NonNull AreaEmergenza a) {
        d.setArea(a);

        Intent intent;
        if (mapping.containsKey(a.getClass()))
            intent = new Intent(getApplication().getApplicationContext(), mapping.get(a.getClass()));
        else
            intent = new Intent(getApplication().getApplicationContext(), DettagliView.class);
        getApplication().startActivity(intent);
    }

    @Override
    public void onChanged(@Nullable Mappa mappa) {
        Log.v("MappaViewModelBase::onChanged(Mappa)", "Mappa cambiata: " + mappa);
        aggiornaAree();
    }
}
