package it.dsantini.areediattesa.model;

import android.support.annotation.NonNull;

public interface Mappa {
    /**
     * @return Gruppi di aree di attesa contenuti nella mappa
     */
    @NonNull
    GruppoAree[] getGruppi();

    /**
     * @param g Gruppo di aree da aggiungere alla mappa
     * @return Nuova Mappa con il gruppo indicato
     */
    @NonNull
    Mappa con(@NonNull GruppoAree g);

    /**
     * @param g Gruppo di aree da rimuovere dalla mappa
     * @return Nuova Mappa senza il gruppo indicato
     */
    @NonNull
    Mappa senza(@NonNull GruppoAree g);
}
