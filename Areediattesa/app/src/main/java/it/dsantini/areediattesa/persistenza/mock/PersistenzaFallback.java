package it.dsantini.areediattesa.persistenza.mock;

import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.Mappa;
import it.dsantini.areediattesa.model.impl.MappaBase;
import it.dsantini.areediattesa.persistenza.Persistenza;

public class PersistenzaFallback implements Persistenza {
    @Override
    public void salva(@NonNull Mappa m) {

    }

    @NonNull
    @Override
    public Mappa carica() {
        return new MappaBase(new GruppoAree[0]);
    }
}
