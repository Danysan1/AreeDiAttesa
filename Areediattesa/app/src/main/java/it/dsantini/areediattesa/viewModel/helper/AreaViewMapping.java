package it.dsantini.areediattesa.viewModel.helper;

import android.support.annotation.NonNull;

import java.util.Map;

import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.view.DettagliView;

public interface AreaViewMapping {
    @NonNull
    Map<Class<? extends AreaEmergenza>, Class<? extends DettagliView>> getMap();
}
