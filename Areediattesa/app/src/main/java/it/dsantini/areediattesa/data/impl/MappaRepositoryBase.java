package it.dsantini.areediattesa.data.impl;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import it.dsantini.areediattesa.data.MappaRepository;
import it.dsantini.areediattesa.data.Stato;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.Mappa;
import it.dsantini.areediattesa.model.impl.MappaBase;
import it.dsantini.areediattesa.persistenza.Persistenza;

public class MappaRepositoryBase implements MappaRepository {
    private static MappaRepositoryBase instance;

    @NonNull
    private final MutableLiveData<Mappa> mappa;
    @NonNull
    private final MutableLiveData<Stato> stato;

    @Nullable
    private Persistenza persistenza;

    private MappaRepositoryBase() {
        mappa = new MutableLiveData<>();
        stato = new MutableLiveData<>();
        stato.setValue(Stato.non_caricato);
    }

    public static MappaRepositoryBase getInstance() {
        if (instance == null)
            instance = new MappaRepositoryBase();

        return instance;
    }

    public void setPersistenza(@NonNull Persistenza p) {
        if (p == null)
            throw new IllegalArgumentException("Persistenza p null");

        this.persistenza = p;

        stato.setValue(Stato.caricamento);
        new CaricamentoAsincrono().start();
    }

    @NonNull
    @Override
    public LiveData<Mappa> getMappa() {
        return mappa;
    }

    @NonNull
    @Override
    public LiveData<Stato> getStato() {
        return stato;
    }

    @Override
    public void setMappa(Mappa m) {
        mappa.setValue(m);
    }

    @Override
    public void postMappa(Mappa m) {
        mappa.postValue(m);
    }

    @Override
    public void salvaMappa() {
        stato.setValue(Stato.salvataggio);
        new SalvataggioAsincrono().start();
    }

    private class CaricamentoAsincrono extends Thread {
        CaricamentoAsincrono() {
            super(new Runnable() {
                @Override
                public void run() {
                    final String TAG = "CaricamentoAsincrono::target::run";

                    if (persistenza == null)
                        Log.w(TAG, "Persistenza nulla");
                    else {
                        try {
                            Log.v(TAG, "Caricamento in corso");
                            mappa.postValue(persistenza.carica());
                            stato.postValue(Stato.caricato);
                        } catch (Exception e) {
                            Log.e(TAG, "Errore nel caricamento dei gruppi", e);
                            stato.postValue(Stato.errore_caricamento);
                            mappa.postValue(new MappaBase(new GruppoAree[0]));
                        }
                    }
                }
            });
        }
    }

    private class SalvataggioAsincrono extends Thread {
        SalvataggioAsincrono() {
            super(new Runnable() {
                @Override
                public void run() {
                    final String TAG = "SalvataggioAsincrono::target::run";

                    if (mappa.getValue() == null || persistenza == null)
                        Log.w(TAG, "Mappa o Persistenza nulla");
                    else {
                        try {
                            Log.v(TAG, "Salvataggio in corso");
                            // TODO In questo frangente il thread principale potrebbe modifcare mappa.getValue() come null
                            persistenza.salva(mappa.getValue());
                            stato.postValue(Stato.caricato);
                        } catch (Exception e) {
                            Log.e(TAG, "Errore nel salvataggio", e);
                            stato.postValue(Stato.errore_salvataggio);
                        }
                    }
                }
            });
        }
    }
}
