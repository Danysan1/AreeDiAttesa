package it.dsantini.areediattesa.persistenza;

import android.support.annotation.NonNull;
import android.support.test.InstrumentationRegistry;

import it.dsantini.areediattesa.persistenza.impl.PersistenzaRoom;

public class PersistenzaRoomTest extends PersistenzaTest {
    public PersistenzaRoomTest() {
        super(new PersistenzaRoom(InstrumentationRegistry.getTargetContext()));
    }
}
