package it.dsantini.areediattesa.rete.impl.overpass;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface OverpassInterface {
    @GET("interpreter")
    @Headers("Accept:application/json")
    Call<OverpassData> jsonQuery(@Query("data") String data);
}
