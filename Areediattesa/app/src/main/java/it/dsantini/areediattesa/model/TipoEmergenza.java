package it.dsantini.areediattesa.model;

import android.support.annotation.NonNull;

public interface TipoEmergenza {
    /**
     * @return Nome del tipo di emergenza
     */
    @NonNull
    String getNome();
}
