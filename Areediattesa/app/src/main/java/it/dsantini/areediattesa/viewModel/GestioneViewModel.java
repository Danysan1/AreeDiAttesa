package it.dsantini.areediattesa.viewModel;

import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import it.dsantini.areediattesa.data.Stato;
import it.dsantini.areediattesa.model.GruppoAree;

public interface GestioneViewModel {
    /***
     * @return Gruppi osservabili da visualizzare
     */
    @NonNull
    LiveData<GruppoAree[]> getGruppi();

    /***
     * @return Stato osservabile della mappa
     */
    @NonNull
    LiveData<Stato> getStato();

    /***
     * @return Stato osservabile dello scaricamento
     */
    @NonNull
    LiveData<StatoDownload> getStatoDownload();

    void onAggiornaTutti();

    /***
     * @param g Gruppo da aggiornare
     */
    void onAggiorna(@NonNull GruppoAree g);

    void onRimuoviTutti();

    /***
     * @param g Gruppo da rimuovere
     */
    void onRimuovi(@NonNull GruppoAree g);

    /***
     * @param g Gruppo da aggiungere ai preferiti
     */
    void onAggiungiPreferito(@NonNull GruppoAree g);

    /***
     * @param g Gruppo da rimuovere dai preferiti
     */
    void onRimuoviPreferito(@NonNull GruppoAree g);
}
