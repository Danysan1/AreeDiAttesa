
package it.dsantini.areediattesa.rete.impl.overpass;

import android.view.ViewDebug;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Tags {

    @SerializedName("addr:city")
    @Expose
    private String addrCity;
    @SerializedName("addr:street")
    @Expose
    private String addrStreet;
    @SerializedName("emergency")
    @Expose
    private String emergency;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("assembly_point:earthquake")
    @Expose
    private String assemblyPointEarthquake;
    @SerializedName("assembly_point:fire")
    @Expose
    private String assemblyPointFire;
    @SerializedName("assembly_point:flood")
    @Expose
    private String assemblyPointFlood;
    @SerializedName("covered")
    @Expose
    private String covered;
    @SerializedName("capacity")
    @Expose
    private String capacity;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("operator")
    @Expose
    private String operator;
    @SerializedName("source")
    @Expose
    private String source;
    @SerializedName("area")
    @Expose
    private String area;
    @SerializedName("addr:postcode")
    @Expose
    private String addrPostcode;
    @SerializedName("image")
    @Expose
    private String image;

    public String getAddrCity() {
        return addrCity;
    }

    public void setAddrCity(String addrCity) {
        this.addrCity = addrCity;
    }

    public String getAddrStreet() {
        return addrStreet;
    }

    public void setAddrStreet(String addrStreet) {
        this.addrStreet = addrStreet;
    }

    public String getEmergency() {
        return emergency;
    }

    public void setEmergency(String emergency) {
        this.emergency = emergency;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public String getAssemblyPointEarthquake() {
        return assemblyPointEarthquake;
    }

    public void setAssemblyPointEarthquake(String assemblyPointEarthquake) {
        this.assemblyPointEarthquake = assemblyPointEarthquake;
    }

    public String getAssemblyPointFire() {
        return assemblyPointFire;
    }

    public void setAssemblyPointFire(String assemblyPointFire) {
        this.assemblyPointFire = assemblyPointFire;
    }

    public String getAssemblyPointFlood() {
        return assemblyPointFlood;
    }

    public void setAssemblyPointFlood(String assemblyPointFlood) {
        this.assemblyPointFlood = assemblyPointFlood;
    }

    public String getCovered() {
        return covered;
    }

    public void setCovered(String covered) {
        this.covered = covered;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAddrPostcode() {
        return addrPostcode;
    }

    public void setAddrPostcode(String addrPostcode) {
        this.addrPostcode = addrPostcode;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
