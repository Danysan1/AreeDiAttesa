package it.dsantini.areediattesa.model.impl;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.util.Optional;

import it.dsantini.areediattesa.model.Fotografia;
import it.dsantini.areediattesa.model.Geometria;
import it.dsantini.areediattesa.model.TipoEmergenza;

@Entity
public class AreaAttesa extends AreaEmergenzaBase {
    @PrimaryKey
    private final int id;
    public static final int DEFAULT_ID = Integer.MIN_VALUE;

    @Nullable
    private final Integer capienza;
    @Nullable
    private final Boolean alCoperto;

    public AreaAttesa(int id,
                      @NonNull String nome,
                      @Nullable String descrizione,
                      @Nullable String gestore,
                      @NonNull TipoEmergenza[] emergenze,
                      @NonNull Fotografia[] fotografie,
                      @NonNull Geometria geometria,
                      @Nullable Integer capienza,
                      @Nullable Boolean alCoperto) {
        super(nome, descrizione, gestore, emergenze, fotografie, geometria);

        if (capienza != null && capienza <= 0)
            throw new IllegalArgumentException("capienza <= 0");

        this.id = id;
        this.capienza = capienza;
        this.alCoperto = alCoperto;
    }

    @Ignore
    public AreaAttesa(@NonNull String nome,
                      @Nullable String descrizione,
                      @Nullable String gestore,
                      @NonNull TipoEmergenza[] emergenze,
                      @NonNull Fotografia[] fotografie,
                      @NonNull Geometria geometria,
                      @Nullable Integer capienza,
                      @Nullable Boolean alCoperto) {
        this(DEFAULT_ID, nome, descrizione, gestore, emergenze, fotografie, geometria, capienza, alCoperto);
    }

    public int getId() {
        return id;
    }

    @NonNull
    public Optional<Integer> getCapienza() {
        return Optional.ofNullable(capienza);
    }

    @NonNull
    public Optional<Boolean> isAlCoperto() {
        return Optional.ofNullable(alCoperto);
    }
}
