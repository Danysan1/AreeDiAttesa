package it.dsantini.areediattesa.rete.impl;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.gson.JsonParseException;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.BoundingBox;
import it.dsantini.areediattesa.model.Fotografia;
import it.dsantini.areediattesa.model.Geometria;
import it.dsantini.areediattesa.model.GruppoAree;
import it.dsantini.areediattesa.model.Nodo;
import it.dsantini.areediattesa.model.TipoEmergenza;
import it.dsantini.areediattesa.model.impl.AreaAttesa;
import it.dsantini.areediattesa.model.impl.FotografiaBase;
import it.dsantini.areediattesa.model.impl.GruppoAreeBase;
import it.dsantini.areediattesa.model.impl.NodoBase;
import it.dsantini.areediattesa.model.impl.Poligono;
import it.dsantini.areediattesa.model.impl.TipoEmergenzaBase;
import it.dsantini.areediattesa.rete.DownloadException;
import it.dsantini.areediattesa.rete.impl.overpass.Element;
import it.dsantini.areediattesa.rete.impl.overpass.OverpassData;
import it.dsantini.areediattesa.rete.impl.overpass.OverpassInterface;
import it.dsantini.areediattesa.rete.impl.overpass.Tags;
import retrofit2.Call;
import retrofit2.Response;

public class SorgenteAreeOverpass extends SorgenteAreeRetrofit {
    private static final String ENDPOINT_DEFAULT = "https://overpass-api.de/api/";

    public SorgenteAreeOverpass(@NonNull String endpoint) {
        super(endpoint);
    }

    public SorgenteAreeOverpass() {
        this(ENDPOINT_DEFAULT);
    }

    @Override
    @NonNull
    public GruppoAree visit(@NonNull BoundingBox b) throws DownloadException {
        final String TAG = "SorgenteAreeOverpass::visit";

        OverpassInterface s = getRetrofit().create(OverpassInterface.class);
        // [out:json][timeout:25]; (node[emergency=assembly_point]({s},{w},{n},{e}); way[emergency=assembly_point]({s},{w},{n},{e}); ); out body; >; out skel qt;
        String bbox = String.format(Locale.US, "%f,%f,%f,%f", b.getLatMin(), b.getLonMin(), b.getLatMax(), b.getLonMax()),
                query = "[out:json][timeout:15]; (node[emergency=assembly_point]("
                        + bbox
                        + "); way[emergency=assembly_point]("
                        + bbox
                        + "); ); out body qt; >; out skel qt;";
        Log.v(TAG, query);
        Call<OverpassData> call = s.jsonQuery(query);
        Response<OverpassData> response;

        try {
            response = call.execute();
        } catch (SocketTimeoutException e) {
            throw new DownloadException("Timeout della chiamata raggiunto", e);
        } catch (IOException e) {
            throw new DownloadException("Errore di I/O durante la chiamata per il download", e);
        } catch (JsonParseException e) {
            throw new DownloadException("Errore nell'interpretazione dei dati ricevuti", e);
        } catch (Exception e) {
            throw new DownloadException("Errore generico durante il download", e);
        }

        if (response.code() != 200)
            throw new DownloadException(response.code());

        AreaEmergenza[] aree;

        if (response.body() == null) {
            aree = new AreaEmergenza[0];
            Log.w(TAG, "Corpo risposta nullo");
        } else
            aree = getAree(response.body().getElements());

        return new GruppoAreeBase(aree, b, false, LocalDateTime.now());
    }

    @NonNull
    private AreaEmergenza[] getAree(@NonNull Collection<Element> elements) {
        final String TAG = "SorgenteAreeOverpass::getAree";

        Set<AreaEmergenza> aree = new HashSet<>();

        for (Element e : elements) {
            Tags t = e.getTags();
            if (t != null && t.getEmergency() != null && t.getEmergency().equals("assembly_point")) {
                Geometria geom = getGeometria(e, elements);
                if (geom != null) {
                    String name = e.getTags().getName();
                    if (name == null)
                        name = "";

                    TipoEmergenza[] emergenze = getTipiEmergenza(t);

                    Fotografia[] foto = getFotografie(t);

                    Integer capienza = null;
                    if (t.getCapacity() != null) {
                        try {
                            capienza = Integer.parseInt(t.getCapacity());
                        } catch (NumberFormatException ex) {
                            Log.e(TAG, "Errore nel parsing della capienza: " + t.getCapacity(), ex);
                        }
                    }
                    Boolean coperto = null;
                    if (t.getCovered() != null)
                        coperto = t.getCovered().equals("yes");

                    try {
                        AreaEmergenza area = new AreaAttesa(name,
                                t.getDescription(),
                                t.getOperator(),
                                emergenze,
                                foto,
                                geom,
                                capienza,
                                coperto);
                        aree.add(area);
                    } catch (IllegalArgumentException ex) {
                        Log.e(TAG, "Creazione AreaAttesa fallita", ex);
                    }
                }
            }
        }

        return aree.toArray(new AreaEmergenza[0]);
    }

    @Nullable
    private Geometria getGeometria(@NonNull Element e, @NonNull Collection<Element> elements) {
        final String TAG = "SorgenteAreeOverpass::getGeometria";

        Geometria geom = null;
        switch (e.getType()) {
            case "node": // Nodo
                geom = new NodoBase(e.getLat(), e.getLon());
                break;
            case "way":
                List<Long> nodi = e.getNodes();
                if (!nodi.get(0).equals(nodi.get(nodi.size() - 1))) // Ultimo nodo diverso dal primo -> Linea
                    Log.e(TAG, "Trovata una linea, la scarto");
                else { // Ultimo nodo uguale al primo -> Poligono
                    List<Nodo> vertici = new ArrayList<>();
                    for (int i = 0; i < e.getNodes().size() - 1; i++) {
                        Long id = e.getNodes().get(i);
                        for (Element nodo : elements)
                            if (nodo.getId().equals(id) && nodo.getType().equals("node"))
                                vertici.add(new NodoBase(nodo.getLat(), nodo.getLon()));
                    }

                    geom = new Poligono(vertici.toArray(new Nodo[0]));
                }
                break;
            default:
                Log.e(TAG, "Tipo sconosciuto trovato: " + e.getType());
        }
        return geom;
    }

    @NonNull
    private TipoEmergenza[] getTipiEmergenza(@NonNull Tags t) {
        Set<TipoEmergenza> emergenze = new HashSet<>();

        if (t.getAssemblyPointEarthquake() != null
                && t.getAssemblyPointEarthquake().equals("yes"))
            emergenze.add(TipoEmergenzaBase.TERREMOTO);
        if (t.getAssemblyPointFire() != null
                && t.getAssemblyPointFire().equals("yes"))
            emergenze.add(TipoEmergenzaBase.INCENDIO);
        if (t.getAssemblyPointFlood() != null
                && t.getAssemblyPointFlood().equals("yes"))
            emergenze.add(TipoEmergenzaBase.ALLUVIONE);
        //TODO altri tipi

        return emergenze.toArray(new TipoEmergenza[0]);
    }

    @NonNull
    private Fotografia[] getFotografie(@NonNull Tags t) {
        final String TAG = "SorgenteAreeOverpass::getFotografie";
        Set<Fotografia> foto = new HashSet<>();

        String image = t.getImage();
        if (image != null && (image.endsWith("jpg")
                || image.endsWith("jpeg")
                || image.endsWith("png"))) {
            try {
                URL url = new URL(image);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setDoInput(true);
                connection.connect();
                InputStream in = connection.getInputStream();
                Bitmap bitmap = BitmapFactory.decodeStream(in);
                in.close();
                Drawable drawable = new BitmapDrawable(bitmap);
                foto.add(new FotografiaBase(drawable, null));

//                File directory= Environment.getExternalStorageDirectory();
//                String fileName = image.substring(image.lastIndexOf('/') + 1);
//                File imageFile = new File(directory, fileName);
//                FileOutputStream out = new FileOutputStream(imageFile);
//                bitmap.compress(Bitmap.CompressFormat.PNG, 50, out);
//                out.flush();
//                out.close();
            } catch (MalformedURLException e) {
                Log.e(TAG, "URL image malformato", e);
            } catch (IOException e) {
                Log.e(TAG, "Connessione fallita per la fotografia " + image, e);
            } catch (Exception e) {
                Log.e(TAG, "Download fallito per l'immagine " + image, e);
            }
        }

        // TODO wikimedia_commons=*

        return foto.toArray(new Fotografia[0]);
    }
}
