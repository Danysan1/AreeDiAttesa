package it.dsantini.areediattesa.model.impl;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.time.LocalDateTime;

import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.Confine;
import it.dsantini.areediattesa.model.GruppoAree;

@Entity
public class GruppoAreeBase implements GruppoAree {
    @PrimaryKey
    private final int id;
    public static final int DEFAULT_ID = Integer.MIN_VALUE;

    @NonNull
    private final AreaEmergenza[] aree;

    @NonNull
    private final Confine confine;

    private final boolean preferito;

    @NonNull
    private final LocalDateTime timestamp;

    public GruppoAreeBase(int id,
                          @NonNull AreaEmergenza[] aree,
                          @NonNull Confine confine,
                          boolean preferito,
                          @NonNull LocalDateTime timestamp) {
        if (aree == null)
            throw new IllegalArgumentException("Aree nullo");
        if (confine == null)
            throw new IllegalArgumentException("Confine nullo");
        if (timestamp == null)
            throw new IllegalArgumentException("Timestamp nullo");

        for (int i = 0; i < aree.length; i++) {
            if (aree[i] == null)
                throw new IllegalArgumentException("aree[" + i + "] nullo");

            for (int j = 0; j < aree.length; j++)
                if (i != j && aree[i] == aree[j])
                    throw new IllegalArgumentException("aree[" + i + "] uguale a aree[" + j + "]");
        }

        this.id = id;
        this.aree = aree;
        this.confine = confine;
        this.preferito = preferito;
        this.timestamp = timestamp;
    }

    @Ignore
    public GruppoAreeBase(@NonNull AreaEmergenza[] aree,
                          @NonNull Confine confine,
                          boolean preferito,
                          @NonNull LocalDateTime timestamp) {
        this(DEFAULT_ID, aree, confine, preferito, timestamp);

    }

    public int getId() {
        return id;
    }

    @Override
    @NonNull
    public AreaEmergenza[] getAree() {
        return aree;
    }

    @Override
    @NonNull
    public Confine getConfine() {
        return confine;
    }

    @Override
    public boolean isPreferito() {
        return preferito;
    }

    @Override
    @NonNull
    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    @NonNull
    @Override
    public GruppoAree withPreferito(boolean p) {
        if (isPreferito() == p)
            return this;
        else
            return new GruppoAreeBase(getAree(), getConfine(), p, getTimestamp());
    }

    @Override
    public String toString() {
        StringBuilder b = new StringBuilder();
        b.append("GruppoAreeBase con ");
        b.append(getAree().length);
        b.append(" aree del ");
        b.append(getTimestamp());
        if (isPreferito())
            b.append(", preferito");
        return b.toString();
    }
}
