package it.dsantini.areediattesa.model.impl;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.Geometria;
import it.dsantini.areediattesa.model.Nodo;
import it.dsantini.areediattesa.view.helper.GeometriaPainter;

@Entity
public class Poligono implements Geometria {
    @PrimaryKey
    private final int id;
    public static final int DEFAULT_ID = Integer.MIN_VALUE;

    @NonNull
    private final Nodo[] vertici;

    public Poligono(int id, @NonNull Nodo[] vertici) {
        if (vertici == null)
            throw new IllegalArgumentException("vertici == null");

        if (vertici.length < 3)
            throw new IllegalArgumentException("vertici ha meno di 3 elementi");

        for (int i = 0; i < vertici.length; i++) {
            if (vertici[i] == null)
                throw new IllegalArgumentException("vertici[" + i + "] == null");

            for (int j = i + 1; j < vertici.length; j++)
                if (vertici[i] == vertici[j])
                    throw new IllegalArgumentException("vertici[" + i + "] == vertici[" + j + "]");
        }

        this.id = id;
        this.vertici = vertici;
    }

    @Ignore
    public Poligono(@NonNull Nodo[] vertici) {
        this(DEFAULT_ID, vertici);
    }

    public int getId() {
        return id;
    }

    @NonNull
    public Nodo[] getVertici() {
        return vertici;
    }

    @NonNull
    @Override
    public Nodo getPuntoSuSuperficie() {
        return vertici[0];
    }

    @Override
    public void accept(GeometriaPainter gp) {
        gp.stampa(this);
    }
}
