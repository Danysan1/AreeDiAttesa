package it.dsantini.areediattesa.persistenza;

import android.support.annotation.NonNull;

import it.dsantini.areediattesa.model.Mappa;

public interface Persistenza {
    /***
     * @param m Mappa da salvare nell'archiviazione
     */
    void salva(@NonNull Mappa m);

    /***
     * @return Mappa caricata dall'archiviazione
     */
    @NonNull
    Mappa carica();
}
