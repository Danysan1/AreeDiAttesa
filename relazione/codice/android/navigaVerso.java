float lat = ..., // Latitudine
	lon = ...; // Longitudine
Uri uri = Uri.parse("google.navigation:q=" + lat + "," + lon);
Intent intent = new Intent(Intent.ACTION_VIEW, uri);
startActivity(intent);
