package it.dsantini.areediattesa.rete;

import android.support.annotation.NonNull;

public class DownloadException extends Exception {
    public DownloadException(int httpCode) {
        super("Codice HTTP " + httpCode);
    }

    public DownloadException(@NonNull String spiegazione, @NonNull Exception e){
        super(spiegazione,e);
    }
}
