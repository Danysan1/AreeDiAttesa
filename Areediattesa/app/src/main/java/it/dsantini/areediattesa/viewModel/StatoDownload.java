package it.dsantini.areediattesa.viewModel;

public enum StatoDownload {
    scaricato,
    scaricamento_in_corso,
    scaricamento_fallito,
    area_troppo_grande;
}
