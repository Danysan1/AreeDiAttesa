package it.dsantini.areediattesa.view;

import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Locale;
import java.util.Optional;

import it.dsantini.areediattesa.R;
import it.dsantini.areediattesa.model.AreaEmergenza;
import it.dsantini.areediattesa.model.impl.AreaAttesa;

public class AreaAttesaView extends DettagliView {
    @Override
    protected void stampaArea(@NonNull AreaEmergenza area) {
        super.stampaArea(area);
        final String TAG = "AreaAttesaView::stampaArea";

        if (!(area instanceof AreaAttesa))
            Log.e(TAG, "Area non istanza AreaAttesa, non faccio nulla");
        else {
            AreaAttesa aa = (AreaAttesa) area;
            LinearLayout elencoExtra = findViewById(R.id.elencoExtra);

            Optional<Integer> optCapienza = aa.getCapienza();
            if (optCapienza.isPresent()) {
                TextView capienza = (TextView) getLayoutInflater().inflate(R.layout.dettagli_text_view,elencoExtra);
                capienza.setText(String.format(Locale.getDefault(), getString(R.string.capienza), optCapienza.get()));
                elencoExtra.addView(capienza);
            }

            Optional<Boolean> optCoperto = aa.isAlCoperto();
            if (optCoperto.isPresent()) {
                TextView coperto = (TextView) getLayoutInflater().inflate(R.layout.dettagli_text_view,elencoExtra,false);
                coperto.setText(optCoperto.get() ? getString(R.string.al_coperto) : getString(R.string.all_aperto));
                elencoExtra.addView(coperto);
            }
        }
    }
}
